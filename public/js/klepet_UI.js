/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 * 
 * 
 */
 
 var vzdevki = [];

 
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  console.log("zazene f");
  if(vhodnoBesedilo.indexOf('http') > -1 && (vhodnoBesedilo.indexOf('.jpg') > -1 ||vhodnoBesedilo.indexOf('.gif') > -1 || vhodnoBesedilo.indexOf('.png') > -1)) {
    console.log("pa tud pogoj izpolne");
    var novIzhod = vhodnoBesedilo.split("http").join("<img src='http")
                  .split(".jpg").join(".jpg' style='padding-left:20px;width:200px' />")
                  .split(".gif").join(".gif' style='padding-left:20px;width:200px' />")
                  .split(".png").join(".png' style='padding-left:20px;width:200px' />");
    
   /* var novIzhod = vhodnoBesedilo.replace("https", "<img src='https");
    novIzhod = vhodnoBesedilo.replace(".jpg", ".jpg' style='padding-left:20px;width:200px'>");
    novIzhod = vhodnoBesedilo.replace(".png", ".png' style='padding-left:20px;width:200px'>");
    novIzhod = vhodnoBesedilo.replace(".gif", ".gif' style='padding-left:20px;width:200px'>");
    */
    console.log(novIzhod);
    return novIzhod;
  }
  console.log(vhodnoBesedilo);
  return vhodnoBesedilo;
}
/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = (sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1) 
  || (sporocilo.indexOf('http') > -1 && (sporocilo.indexOf('.jpg') > -1 ||sporocilo.indexOf('.gif') > -1 || sporocilo.indexOf('.png') > -1));
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<br /><img")
               .split("png' /&gt;").join("png' />")
               .split("gif' &gt;").join("gif' />")
               .split("jpg' /&gt;").join("jpg' />")
               .split("200px' /&gt;").join("200px' />");

               
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
// Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    if(sporocilo.indexOf('/preimenuj') == 0) {
      console.log("amm ja index je " + sporocilo.indexOf('/preimenuj'));
      var singleObject = {};
      var parametri = sporocilo.split('"');
      if(parametri) {
        console.log("printam uporabnika " + parametri[1]);
        console.log("printam vzdevek " + parametri[3]);

        singleObject["uporabnik"] = parametri[1];
        singleObject["vzdevek"] = parametri[3];
        vzdevki.push(singleObject);
      }
    } else {  
      sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    }
    if (sistemskoSporocilo) {
      sistemskoSporocilo = spremeniIme(sistemskoSporocilo);
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    sporocilo.besedilo = spremeniIme(sporocilo.besedilo);
    if(sporocilo.besedilo.indexOf('&#9756@ZZZ4589') != -1) {
      console.log("sporocilo je bilo poslano(hetml)");
      var novoB = sporocilo.besedilo.split('@ZZZ4589').join(" ");
      var novElement = divElementHtmlTekst(novoB);
      $('#sporocila').append(novElement);
    } else {
      console.log("sporocilo je bilo poslano(navadno)");
      var novElement = divElementEnostavniTekst(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }
  })

  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var novUporabnik = dodajVzdevek(uporabniki[i]);
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(novUporabnik));
    }
     $('#seznam-uporabnikov div').click(function() {
      var input = '/zasebno "' + $(this).text() + '" "&#9756@ZZZ4589"';
      input = spremeniImeObratno(input);
      var sistemskoSporocilo = klepetApp.procesirajUkaz(input);
      var sistemskoSporociloX = sistemskoSporocilo.split('@');
      sistemskoSporociloX[0] = spremeniIme(sistemskoSporociloX[0]);
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporociloX[0]));
      console.log("clikc");
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
  function dodajVzdevek(uporabnik) {
    //console.log("ta funkcija se kr naprej klice");
    for(var i = 0; i < vzdevki.length; i++) {
      if(vzdevki[i].uporabnik == uporabnik) {
        var izhod = vzdevki[i].vzdevek + " (" + uporabnik + ")"; 
      //  console.log(izhod + " tole sm zej zmutil skp");
        return izhod;
      }
    }
    return uporabnik;
  }
});

function spremeniIme(besedilo) {
  console.log("zamenjava se izvede");
    for(var i = 0; i < vzdevki.length; i++) {
      if(besedilo.indexOf(vzdevki[i].uporabnik) > -1) {
        console.log("tud najde match");
        var besede = besedilo.split(vzdevki[i].uporabnik);
        var izhod = besede.join(vzdevki[i].vzdevek+" ("+vzdevki[i].uporabnik+")");
        besedilo = izhod;
        console.log("toel nardi "+izhod);
      }
    }
    return besedilo;
}
function spremeniImeObratno(besedilo) {
  console.log("obratna zamenjava se izvede");
    for(var i = 0; i < vzdevki.length; i++) {
      if(besedilo.indexOf(vzdevki[i].uporabnik) > -1) {
        console.log("tud najde match");
        var besede = besedilo.split(vzdevki[i].vzdevek+" ("+vzdevki[i].uporabnik+")");
        var izhod = besede.join(vzdevki[i].uporabnik);
        besedilo = izhod;
        console.log("toel nardi "+izhod);
      }
    }
    return besedilo;
}